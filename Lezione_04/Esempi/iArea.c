#include <stdio.h>

int main(void)
{
	int area, base, altezza;
	
	/* I valori di base e altezza sono fissati */
	base = 3; 
	altezza = 5;
	
	/* Assegna alla variabile area il prodotto di base e altezza */
	area = base * altezza;
	
	/* Stampa il risultato a video */
	printf("Area = %d\n", area);
	
	return 0;
}
