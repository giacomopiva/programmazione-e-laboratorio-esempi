#include <stdio.h>

int main(void)
{
	int somma, differenza, prodotto, quoziente, resto;
	int op1, op2;
	
	// Somma
	op1 = 2;
	op2 = 3;
	somma = op1 + op2;
	
	printf("La somma di %d e %d è: %d\n", op1, op2, somma);
		
	
	// Differenza
	op1 = 8;
	op2 = 3;
	differenza = op1 - op2;
	
	printf("La differenza fra %d e %d è: %d\n", op1, op2, differenza);
		
	
	// Prodotto
	op1 = 6;
	op2 = 3;
	prodotto = op1 * op2;
	
	printf("Il prodotto fra %d e %d è: %d\n", op1, op2, prodotto);
	
	
	// Quoziente
	op1 = 5;
	op2 = 3;
	quoziente = op1 / op2;
	
	printf("La divisione intera fra %d e %d è: %d\n", op1, op2, quoziente);
	
	
	// Resto della divisione intera
	op1 = 5;
	op2 = 3;
	resto = op1 % op2;
	
	printf("Il resto della divisione intera fra %d e %d è: %d\n", op1, op2, resto);
	
	return 0;
}
