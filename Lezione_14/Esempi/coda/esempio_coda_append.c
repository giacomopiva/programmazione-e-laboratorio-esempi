/**
 * Autore: Giacomo Piva <giacomo.piva@unife.it>
 * Descrizione: In questo esempio è implementata
 * la funzione append() che aggiunge un nuovo
 * nodo in fondo alla lista. 
 */

#include <stdio.h>
#include <stdlib.h>

#define MAX_DIM 5

typedef struct _nodo {
	int val;
	struct _nodo *next;
} nodo;

void visualizza(nodo*);
void append(nodo**, int);

int main()
{
	nodo *coda;
	int i, array[MAX_DIM] = {1,2,3,4,5};
	
	// Inizializzo il puntatore della testa a NULL
	coda = NULL;
	
	// Riempio la lista con gli elementi presenti nell'array	
	for (i=0; i<MAX_DIM; i++) {	
		append(&coda, array[i]);
		visualizza(coda);
	}
	
	return 0;
}

void append(nodo **testa, int value)
{		
	nodo *new, *p;

	// Creo un nuovo nodo
	new = (nodo*) malloc(sizeof(nodo));
	new->val = value;
	new->next = NULL;

	// Sto allocando il primo nodo
	if (*testa == NULL) {
		*testa = new;
	} else {
		// Mi posiziono in fondo alla coda
		p = *testa;
		while(p->next != NULL) {
			p = p->next; 
		}
		// Collego l'ultimo elemento al nuovo elemento
		p->next = new;
	}		
}

// Visualizzo la lista
void visualizza(nodo *l)
{
	while(l != NULL) { 	
		printf("%d -> ", l->val); 
	  	l = l->next; 
	 }
	 printf("NULL\n");
}
