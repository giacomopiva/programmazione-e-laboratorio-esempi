/**
 * Autore: Giacomo Piva <giacomo.piva@unife.it>
 * Descrizione: In questo esempio viene 
 * implementata la funzione push() per aggiungere
 * un nuovo nodo in cima allo stack.
 */

#include <stdio.h>
#include <stdlib.h>

#define MAX_DIM 5

typedef struct _nodo {
	int val;
	struct _nodo *next;
} nodo;

void visualizza(nodo *);
void push(nodo**, int);

int main()
{
	nodo *pila;
	int i, array[MAX_DIM] = {1,2,3,4,5};
	
	pila = NULL;
	
	for (i=0; i<MAX_DIM; i++) {
		push(&pila, array[i]);
		visualizza(pila);
	}

	return 0;
}

void push(nodo **testa, int value) 
{
	// Eseguo un semplice inserimento in testa
	nodo *new;
	new = (nodo*) malloc(sizeof(nodo));	
	new->val = value;
	new->next = *testa;
	*testa = new;
}

void visualizza(nodo *testa)
{
	while(testa != NULL) { 	
		printf("%d -> ", testa->val); 
	 	testa = testa->next; 
	 }
	 printf("NULL\n");
}
