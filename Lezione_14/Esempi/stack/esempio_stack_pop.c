/**
 * Autore: Giacomo Piva <giacomo.piva@unife.it>
 * Descrizione: In questo esempio viene
 * implementata la funzione pop() che estrae
 * l'elemento in cima allo stack. Viene inoltre
 * riempito lo stack con la funzione push().
 */

#include <stdio.h>
#include <stdlib.h>

#define MAX_DIM 5

typedef struct _nodo {
	int val;
	struct _nodo *next;
} nodo;

void visualizza(nodo *);
void push(nodo**, int);
nodo* pop(nodo**);

int main()
{
	nodo *stack, *p;
	int array[MAX_DIM] = {1,2,3,4,5};
	int i, todel;
	
	stack = NULL;
	
	for (i=0; i<MAX_DIM; i++) {
		push(&stack, array[i]);
		visualizza(stack);		
	}
	
	p = pop(&stack);	// Estraggo un primo elemento ma NON lo visualizzo
	p = pop(&stack);	// Estraggo un secondo elemento 
	
	if (p != NULL) {
		printf("\nElemento estratto: ");
		visualizza(p);	

		printf("Lo stack asesso è:\n");
		visualizza(stack);	
	} 
	
	return 0;
}

nodo* pop(nodo **testa)
{
	nodo *p, *q;
	p = NULL;
	
	if (*testa != NULL) {
		p = (nodo*) malloc(sizeof(nodo));
		if (p != NULL) {
			p->val = (*testa)->val;
			p->next = NULL;
			q = *testa;
			*testa = (*testa)->next;
			free(q); // Cancello il vecchio nodo
		} else {
			printf("Errore durante l'allocazione della memoria");
		}	
	} else {
		printf("Non ci sono elementi nello stack !!!\n");
	}
	return p; // restituisco il nuovo elemento creato
}

void push(nodo **testa, int value) 
{
	nodo *new;
	new = (nodo*) malloc(sizeof(nodo));	
	new->val = value;
	new->next = *testa;
	*testa = new;
}

void visualizza(nodo *testa)
{
	while(testa != NULL) { 	
		printf("%d -> ", testa->val); 
	 	testa = testa->next; 
	 }
	 printf("NULL\n");
}
