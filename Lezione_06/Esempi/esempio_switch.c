#include <stdio.h>

int main (void)
{  
	int base, altezza;
	float area;
	char forma;
	
	printf("Inserisci la dimensione della base: ");
	scanf("%d", &base);
	
	printf("Inserisci la dimensione dell' altezza: ");
	scanf("%d", &altezza);
	
	printf("Inserisci la forma geometrica triangolo o rettangolo (t/r): ");
	scanf(" %c", &forma);
	
	switch(forma) {
		case 't':
			area = (base*altezza)/2;
		break;
		
		case 'r':
			area = base*altezza;
		break;			
		
		default:
			printf("Non hai inserito una scelta valida.\n");
			return 1;
	}
	
	printf("L'area è: %.2f\n", area);
	
	return 0;
}
