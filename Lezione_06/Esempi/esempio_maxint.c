#include <stdio.h>
#include <limits.h>

/** 
 * INT_MAX, insieme ad altri valori è definito in limits.h e definisce quanto è grande il 
 * massimo numero rappresentabile con una variabile di tipo int. 
 * Altri valori sono: INT_MIN, UINT_MAX, SHRT_MAX, LONG_MIN, LONG_MAX
 */
	
int main (void)
{  
	int i = INT_MAX; 
	printf("i = %d \n\n", i);			
	
	// Cosa succede se eccediamo il valore di MAX_INT?
	i++;
	printf("i = %d \n");	
	
	i++;
	printf("i = %d \n");
	
	i++;
	printf("i = %d \n");
	
	printf("...\n");
	
	return 0;
}
