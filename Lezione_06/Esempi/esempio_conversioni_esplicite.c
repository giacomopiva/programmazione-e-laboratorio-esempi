#include <stdio.h>

int main (void)
{  
	int an_int;
	float a_float;

	// Conversione esplicita (cc -Wconversion)
	// Assegno i valori alle variabili e li visualizzo
	an_int = 0;
	a_float = 123.45;
	
	printf("an_int = %d \n", an_int);
	printf("a_float = %.2f \n", a_float);
	
	// Faccio qualche conversione 
	an_int = a_float; 			// Usando il flag -Wconversion il compilatore si arrabbia
	
	an_int = (int) a_float; 	// Il compilatore non si arrabbia
	
	printf("an_int = %d \n\n\n", an_int);
		
	return 0;
}
