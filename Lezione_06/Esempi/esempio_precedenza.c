#include <stdio.h>

int main (void)
{  
	int risultato; 
	
	risultato = 4 + 5 * 7 + 3;
	printf("4 + 5 * 7 + 3 = %d \n\n", risultato);

	risultato = 4 + ((5 * 7) + 3);
	printf("4 + ((5 * 7) + 3) = %d \n\n", risultato);
	
	risultato = (4 + 5) * (7 + 3);
	printf("(4 + 5) * (7 + 3) = %d \n\n", risultato);
	
	int a = 1;
	int b = 0;
	int c = 0;
	
	if (a || b && c) {
		printf("Yes\n\n");
	} else {
		printf("No\n\n");		
	}
	
	if (a || (b && c)) {
		printf("Yes\n\n");
	} else {
		printf("No\n\n");		
	}
	
	if ((a || b) && c) {
		printf("Yes\n\n");
	} else {
		printf("No\n\n");		
	}
		
	return 0;
}
