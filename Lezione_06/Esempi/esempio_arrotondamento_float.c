#include <stdio.h>

int main (void)
{  

	float ff;
	ff = 1.542342;						// assegno un numero di decimali corretto

	// Troncamento
	printf("ff / 5 = %f \n\n", ff/5); 	// 0.3084684

	// Arrotondamento
	printf("ff / 7 = %f \n\n", ff/7); 	// 0.22033457
	
	return 0;
}
