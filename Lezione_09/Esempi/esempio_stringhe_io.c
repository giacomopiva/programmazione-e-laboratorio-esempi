#include <stdio.h>

int main()
{
  	char word[10]; 
	int i;

	// N.B: La funzione scanf() con le stringhe, 
	// legge solamente fino al return
	// Se la parola inserita è più lunga della 
	// capienza della stringa il comportamento 
	// è indeterminato
	
	// Leggo una parola con il %s
	printf("Inserisci la parola: ");
	scanf("%s", word);
	
	printf("%s \n", word);
		
  return 0;
}
