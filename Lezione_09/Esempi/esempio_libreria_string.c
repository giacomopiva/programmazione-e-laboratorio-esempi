#include <stdio.h>
#include <string.h>

int main()
{
	char s1[20];
	char s2[10];
	char s3[10];
	
	// strcpy
	strcpy(s1, "Ciao");
	printf("Visualizzo s1: %s\n",s1);
	
	strcpy(s2, " Mondo");
	printf("Visualizzo s2: %s\n\n",s2);
	
	// strcat
	strcat(s1, s2);
	printf("Visualizzo s1: %s\n\n",s1);
	
	// strlen
	printf("La lunghezza di s1 ora è: %ld\n\n", strlen(s1)); 

	// strcpy
	strcpy(s3, "Hello");
	printf("Visualizzo s3: %s\n\n",s3);
	
	// strcmp
	printf("Confronto s1 (%s) con s3 (%s): %d\n", s1, s3, strcmp(s1, s3));
	printf("Confronto s1 (%s) con s1 (%s): %d\n", s1, s1, strcmp(s1, s1));
	printf("Confronto s3 (%s) con s1 (%s): %d\n", s3, s1, strcmp(s3, s1));
	
  return 0;
}
