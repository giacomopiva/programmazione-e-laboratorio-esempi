#include <stdio.h>

int main()
{
	char hello[] = "Hello World"; 
	int i;
	char c;	
	
	// Stampo la stringa con il formattatore %s 
	printf("Stampo la stringa con il formattatore:\n");
	printf("%s\n\n", hello);
	
	// Stampo i primi 10 caratteri della stringa
	// usando un ciclo for
	printf("Stampo i primi 10 caratteri della stringa:\n");
	for (i=0; i<10; i++) {
		printf("%c", hello[i]);
	}

	printf("\n\n");

	// Stampo la stringa usando un ciclo while fino al terminatore
	printf("Stampo la stringa usando un ciclo while fino al terminatore:\n");	
	
	i=0;
	while (hello[i] != '\0') {
		printf("%c", hello[i]);
		i++;
	}
	
	printf("\n");

  return 0;
}
