#include <stdio.h>

int main()
{
	// Dichiaro due array e due puntatori
	int a[10];
	char b[10];
	int *pi;
	char *pc;
	
	// Al puntatore ad intero (pi) assegno 
	// il valore del base address dell'array a
	pi = a;
	
	// Al puntatore a carattere (pc) assegno 
	// il valore del base address dell'array b
	pc = b;

	pi = pi+3; // significa spostare avanti pi di 3 posizioni 
			   // dove ogni posizione occupa lo spazio di un int

	pc = pc+3; // significa spostare avanti pi di 3 posizioni 
			   // dove ogni posizione occupa lo spazio di un char

	printf("a: %p\n\n",&a);
	printf("a+3: %p\n\n",pi);
	
	printf("b: %p\n\n",&b);
	printf("b+3: %p\n\n",pc);

	return 0;
}
