#include <stdio.h>

#define N 5

void visualizza_array(int *array);
void visualizza_elemento(int *elemento);

int main(void)
{
	int vett[N] = { 3, 6, 7, 5, 2 };	
	int i;
	
	visualizza_array(vett);
	
	for (i=0; i<N; i++) {
		visualizza_elemento(&vett[i]);
	}

	return 0;
}

void visualizza_array(int *array)
{
	int i;
	for (i=0; i<N; i++) {
		printf("%d ", array[i]);
	}
	printf("\n");		
}

void visualizza_elemento(int *elemento) 
{
	printf("%d ", *elemento);
}
