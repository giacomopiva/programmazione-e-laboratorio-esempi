#include <stdio.h>

int main()
{
		int a, b;
		int *p; 
		
		// Assegno ad a un valore ...
		a = 3;
		
		// 1: Assegno a b lo stesso valore di a
		b = a;
		printf("1) a = %d, b = %d\n\n", a, b);

		// 2: Modifico il valore di b
		b = 6;
		printf("2) a = %d, b = %d\n\n", a, b);
		
		// 3: Assegno alla variabile puntatore 
		// p l'indirizzo in memoria di a
		p = &a;
		printf("3) a = %d, *p = %d\n\n", a, *p);
		
		// 4: Assegno alla LOCAZIONE IN MEMORIA 
		// PUNTATA DA P il valore 5
		*p = 5;
		printf("4) *p = %d, a = %d\n\n", *p, a);		
		
		return 0;
}
