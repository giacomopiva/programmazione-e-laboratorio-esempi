#include <stdio.h>

/**
 * in tmp salviamo il valore di a (*a è il contenuto della locazione puntata da a)
 * copiamo il valore contenuto nella locazione puntata da b nella locazione puntata da a 
 * copiamo il contenuto della locazione puntata da b il valore di tmp  
 */
void scambia(int *a, int *b) 
{
    int tmp;	
    tmp = *a;	
    *a = *b;	
    *b = tmp; 
}

int main()
{
    int num1, num2;
	printf("num1 = ");
	scanf("%d", &num1);
	printf("num2 = ");
	scanf("%d", &num2);
	
	scambia(&num1, &num2);
	
	printf("\nnum1 = %d, num2 = %d\n", num1, num2);
	return 0;
}
