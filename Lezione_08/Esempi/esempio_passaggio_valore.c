#include <stdio.h>

void scambia(int a, int b) 
{
    int tmp;
    
		tmp = a;		// salvo a in una var temporanea
    
		a = b;			// assegno ad a il valore di b
    
		b = tmp;		// assegno a b il valore di a, 
								// precedentemente salvato in tmp
}

int main()
{
    int num1, num2;
		printf("num1 = ");
		scanf("%d", &num1);
		printf("num2 = ");
		scanf("%d", &num2);
		
		scambia(num1, num2);
		
		printf("\nnum1 = %d, num2 = %d\n", num1, num2);
		return 0;
}

