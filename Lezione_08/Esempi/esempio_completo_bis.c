#include <stdio.h>

// Prototipo della funzione "potenza" 
int potenza(int, int);

int main()
{
  int a, b, risultato;
  printf("Inserisci il valore di a: ");
  scanf("%d",&a);
  printf("Inserisci il valore di b: ");
  scanf("%d",&b);
  
  risultato = potenza(a,b);
  printf("Il valore di a elevato b è: %d\n", risultato);
  
  risultato = potenza(b,a);
  printf("Il valore di b elevato a è: %d\n", risultato);
  return 0;
}

// Definizione della funzione potenza
int potenza(int x, int y)
{
  int count;
  int r = 1;
  for (count=1; count <= y; count++)
    r = r *  x; 
  return r;
}
