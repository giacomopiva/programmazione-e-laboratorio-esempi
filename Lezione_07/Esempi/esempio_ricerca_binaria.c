#include <stdio.h>

#define N 10

int main()
{
	int vett[N] = {1,2,3,4,5,6,7,8,9,10};
  int i, first, last, middle, search;
	
	// Visualizzo l'array
	for (i=0; i<N; i++)
		printf("%d ", vett[i]);
	printf("\n");
	  
  printf("Inserisci il valore da ricercare: ");
  scanf("%d", &search);
 
  // ricavo gli indici per il primo, l'ultimo e l'elemento centrale
  first = 0;
  last = N - 1;
  middle = (first+last)/2;
		
  while(first <= last) {
		if (vett[middle] == search) {
					printf("\nHo trovato %d all'indice %d.\n", search, middle);
					break;
		} else {
			// sposto il limite del sottovettore in cui effettuo la ricerca
			if (vett[middle] < search) 
				first = middle + 1;    
			else 
				last = middle - 1;

			// in ogni caso ricalcolo l'elemento centrale
			middle = (first + last)/2;
		}
  }
  if ( first > last ) {
     printf("%d non esiste all'interno dell'array.\n", search);
  }
		
	return 0;
}
