#include <stdio.h>

#define N 5	

int main (void)
{  
	// Dichiaro il mio array e contestualmente gli assegno 
	// gli N valori.
	// Posso NON dichiarare la dimensione perchè il 
	// compilatore, sa quanti valori sono e da la dimensione 
	// all'array da solo
	float temperature[] = { 2.0, 3.0, 5.0, 4.0, 2.0 }
	float somma, temp_media, max_temp, min_temp;
	int i;

	for (i=0; i<N; i++) {
		printf("Inserisci il valore %d: ", i+1);
		scanf("%f", &temperature[i]);	
	}

	printf("Temperature inserite:\n");
	for (i=0; i<N; i++) {
		printf("%.1f ", temperature[i]);
	}

	somma 		= temperature[0];
	max_temp 	= temperature[0];
	min_temp 	= temperature[0];
	
	for (i=1; i<N; i++) {
		somma += temperature[i]; 

		// aggiorno la temp massima
		max_temp = temperature[i] > max_temp ? temperature[i] : max_temp;

		// È come scrivere
		// if (temperature[i] > max_temp) {
		//	 max_temp = temperature[i] 
		// } else {
		//   max_temp = max_temp;
	    // }
		
		// aggiorno la temp minima
		min_temp = temperature[i] < min_temp ? temperature[i] : min_temp;

		// È come scrivere
		// if (temperature[i] < min_temp) {
		//	 min_temp = temperature[i] 
		// } else {
		//	 min_temp = min_temp;
		// }		
	}
	
	temp_media = somma/N;
	
	printf("\nTemperatura massima: %.1f\n", max_temp);
	printf("Temperatura minima: %.1f\n",	min_temp);
	printf("Temperatura media: %.2f\n",		temp_media);
	
	return 0;
}
