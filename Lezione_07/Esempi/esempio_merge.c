#include <stdio.h>

#define N 5
#define M 3 

int main()
{
	int vetta[N] = {4,7,2,9,3};
	int vettb[M] = {8,1,6};
	int vettc[M+N];
	int i;
	
	// Visualizzo l'array prima dell'ordinamento
	for (i=0; i<N; i++) {
		printf("%d ", vetta[i]);
	}
	printf("\n");

	for (i=0; i<M; i++) {
		printf("%d ", vettb[i]);
	}
	printf("\n");
	
	i=0;
	while(i<N) {
		vettc[i] = vetta[i];
		i++;
	}

	i=0;
	while(i<M) {
		vettc[N+i] = vettb[i];
		i++;
	}

	printf("\n");

	for (i=0; i<N+M; i++) {
		printf("%d ", vettc[i]);
	}
	printf("\n");
		
	return 0;
}
