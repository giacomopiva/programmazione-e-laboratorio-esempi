#include <stdio.h>

#define N 5

int main(void)
{
	int vett[N] = {22, 25, 21, 23, 20};
	int i, j, tmp;
	
	// Visualizzo l'array prima dell'ordinamento
	printf("Inizialmente l'array contiene: ");	
	for (i=0; i<N; i++) {
		printf("%d ", vett[i]);
	}
	printf("\n\n");
	
	// eseguo l'ordinamento
	for (j=0; j < N-1; j++) {
		for (i=j+1; i<N; i++) {
			if (vett[j] > vett[i]) {
				// eseguo lo scambio
				tmp = vett[j];
				vett[j] = vett[i];
				vett[i] = tmp;
			}
		}
		
		// Visualizzo come varia l'array
		printf("Dopo il %d giro:\t",j+1);
		for (i=0; i<N; i++) {
			printf("%d ", vett[i]);
		}
		printf("\n");	
	}
	
	printf("\n");	
	
	return 0;
}
