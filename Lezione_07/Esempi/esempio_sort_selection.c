#include <stdio.h>

#define N 5

int main(void)
{
	int vett[N] = {22, 25, 21, 23, 20};
	int j, i, k, tmp;
	
	// Visualizzo l'array prima dell'ordinamento
	printf("Inizialmente l'array contiene: ");		
	for (i=0; i<N; i++) {
		printf("%d ", vett[i]);
	}
	printf("\n\n");
	
	// Eseguo l'ordinamento
	// Il ciclo più esterno parte dal primo elemento e si ferma al penultimo	
	for (j=0; j < N-1; j++) {
		// in k salvo il valore dell'indice del ciclo più esterno, mi servirà poi...
		k = j;

		// Il ciclo più interno, parte dal secondo elemento e si ferma all'ultimo
		for (i=j+1; i < N; i++) {
			// confronto i due elementi e salvo il valore dell'indice del valore massimo
			if (vett[k] > vett[i]) {
				k = i;
			}
		} // fine ciclo interno (i)
		
		// Se l'indice k (del minimo) è stato spostato, eseguo lo scambio
		if (k != j) {
			tmp = vett[j];
			vett[j] = vett[k];
			vett[k] = tmp;
		}

		// Visualizzo come varia l'array
		printf("Dopo il %d giro:\t",j+1);
		for (i=0; i<N; i++) {
			printf("%d ", vett[i]);
		}
		printf("\n");	

	} // fine ciclo esterno (j)

	printf("\n");	

	return 0;
}

