#include <stdio.h>

#define N 5

int main(void)
{
	int vett[N] = {22, 25, 21, 23, 20};
	int i, j, tmp;

	// Visualizzo l'array prima dell'ordinamento
	printf("Inizialmente l'array contiene: ");		
	for (i=0; i<N; i++) {
		printf("%d ", vett[i]);
	}
	printf("\n\n");

	// Eseguo l'ordinamento
	// il ciclo più esterno va dal primo elemento fino al penultimo
	for (i=0; i < (N-1); i++) {
		// il ciclo più interno va dal primo elemento fino all'elemento N-i-1
		for (j=0; j < (N-i-1); j++) {
			// confronto gli elementi in posizioni adiacenti
			if (vett[j] > vett[j+1]) {
				// scambio gli elementi
				tmp	= vett[j];
				vett[j]	= vett[j+1];
				vett[j+1] = tmp;
			}			
		}
		
		// Visualizzo come varia l'array
		printf("Dopo il %d giro:\t",i+1);
		for (j=0; j<N; j++) {
			printf("%d ", vett[j]);
		}
		printf("\n");						
	}

	printf("\n");		

	return 0;
}
