#include <stdio.h>
#include <stdlib.h>

int main()
{
	const int HEIGHT = 10000;
	const int WIDTH = 10000;

	int **mat;
	int i, j;

	mat = (int **) malloc(sizeof(int *) * HEIGHT);
	
	for (i=0; i < HEIGHT; i++)
		mat[i] = (int *) malloc(sizeof(int) * WIDTH);
			 
	printf("Init... ");
	fflush(stdout);
	
	for (j=0; j < WIDTH; j++) {
		for (i=0; i < HEIGHT; i++)
			mat[i][j] = (i*j)+(j*i);
	}

	printf("Done!\n");

	return 0;
}
