/**
 * Autore: Giacomo Piva <giacomo.piva@unife.it>
 * Descrizione: In questo esempio, si vuole dare 
 * dimostrazione dell'uso della funzione fgets() 
 * della libreria string.h
 * Inserita una stringa, letta con la funzione 
 * fgets() il programma ne ritorna la lunghezza
 */

#include <stdio.h>
#include <string.h>

#define MAXDIM 255

int main()
{
	// Dichiaro una variabile buffer 
	char buff[MAXDIM];
	int len;
		
	printf("Scrivi una frase: ");
	
	if (fgets(buff, MAXDIM+1, stdin) != NULL) {
		// Se la funzione fgets ritorna un puntatore "valido"
		// recupero la lunghezza della stringa con la funzione strlen()
		// -1 perchè la funzione fgets() considera anche l'andata
		// a capo come carattere.
		len = strlen(buff) -1;
		if (len > 0) {
			printf("La frase è: %s \n", buff);
			printf("La frase è lunga: %d caratteri \n", len);
		} else {
			printf("La stringa è vuota.\n");	
		}
		
	} else {
		// Ci si arriva con ctrl+d (EOF)
		printf("c'è stato un errore nella lettura della stringa.\n");
	}
	
	return 0;
}
