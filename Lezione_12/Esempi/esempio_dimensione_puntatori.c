/**
 * Autore: Giacomo Piva <giacomo.piva@unife.it>
 * Descrizione: In questo esempio, attraverso la 
 * funzione sizeof() si vuole valutare la dimensione
 * in memoria di una variabile puntatore.
 */

#include <stdio.h>

int main()
{
	int *pint;
	char *pchar;
	float *pfloat;
	double *pdouble;
	void *p;

	printf("\n");	
	printf("Memoria occupata dal puntatore ad intero:	\t %lu \n",	sizeof(pint));	
	printf("Memoria occupata dal puntatore a carattere:	\t %lu \n",	sizeof(pchar));	
	printf("Memoria occupata dal puntatore a float:		\t %lu \n",	sizeof(pfloat));	
	printf("Memoria occupata dal puntatore a double:	\t %lu \n",	sizeof(pdouble));	
	printf("Memoria occupata dal puntatore a void:		\t %lu \n",	sizeof(p));	
	
	printf("\n");
		
	return 0;
}
