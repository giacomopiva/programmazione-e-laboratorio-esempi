/**
 * Autore: Giacomo Piva <giacomo.piva@unife.it>
 * Descrizione: In questo esempio, si vuole dare 
 * dimostrazione dell'uso della funzione strtoken() 
 * della libreria string.h 
 * Partendo dall'esempio precedente, la frase inserita
 * viene spezzata nelle sue parti, considerando lo spazio
 * come separatore fra le parti.
 */

#include <stdio.h>
#include <string.h>

#define MAXDIM 255

int main()
{
	char buff[MAXDIM];
	char *token;
	int len, i, count;
	
	printf("Scrivi una frase: ");
	
	if (fgets(buff, MAXDIM+1, stdin) != NULL) {
		len = strlen(buff) -1; // -1 per il return
		if (len > 0) {			
			// Se la frase non è vuota
			// Ottengo il primo token
			token = strtok(buff, " \n"); // \n perchè l'ultimo carattere è il CR
			i = 1; 		// Contatore per printf
			count = 0; 	// Contatore token (parole)
			
			// Finche esistono token ...	
			while (token != NULL) {
				// Stampo il token
				printf("%d° token: \"%s\" \n", i++, token);
				// Aumento il contatore dei token
				count++;
				// Ottengo il prossimo token
			  	token = strtok(NULL, " \n");
			}	
				
		} else {
				printf("La stringa è vuota.\n");
		}
	} else {
		printf("c'è stato un errore nella lettura della stringa.\n");
	}
	
	return 0;
}