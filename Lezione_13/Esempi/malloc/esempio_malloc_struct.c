/**
 * Autore: Giacomo Piva <giacomo.piva@unife.it>
 * Descrizione: In questo esempio si vuole
 * dimostrare l'accesso ai campi di una struttura
 * attraverso l'operatore -> 
 */

#include <stdio.h>
#include <stdlib.h>

struct date {
	int day;
	int month;
	int year;
};

int main()
{
	struct date *today;
	
	today = (struct date*) malloc(sizeof(struct date));

	today->day = 30;
	today->month = 11;
	today->year = 2015;
	
	printf("La data è: %d/%d/%d\n", today->day, today->month, today->year);
		 
	return 0;
}
