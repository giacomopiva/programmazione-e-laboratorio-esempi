/**
 * Autore: Giacomo Piva <giacomo.piva@unife.it>
 * Descrizione: In questo esempio, attraverso la 
 * funzione malloc() viene dichiarato un array
 * dinamicamente.
 */

#include <stdio.h>
#include <stdlib.h>

#define MAX_DIM 50000

int main()
{	
	int i, count;
	int *myarray;
	
	myarray = (int *) malloc(sizeof(int) * MAX_DIM);	
	
	if (myarray != NULL) {
		count = 0;
		for(i=0; i<MAX_DIM; i++) {
			myarray[i] = 0;		
			count++;		
		}
	} else {
		printf("Errore nell' allocazione della memoria\n", );
		return -1;
	}
	printf("Ho inizializzato %d elementi\n", count);
	
	return 0;
}
