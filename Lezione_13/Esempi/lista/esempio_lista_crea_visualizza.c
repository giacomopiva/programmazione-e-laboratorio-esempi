/**
 * Autore: Giacomo Piva <giacomo.piva@unife.it>
 * Descrizione: In questo esempio, viene 
 * creata una lista con 2 nodi e ne viene visualizzato
 * in contenuto con una funzione.
 * Viene creata anche una versione ricorsiva della
 * funzione visualizza.
 */

#include <stdio.h>
#include <stdlib.h>

struct nodo {
	int val;
	struct nodo *next;
};

void visualizza(struct nodo *);
void visualizza_r(struct nodo *);

int main()
{
	struct nodo *testa, *new;
		
	new = (struct nodo*) malloc(sizeof(struct nodo)); 
	new->val = 10;
	new->next = NULL;
	testa = new;
	
	new = (struct nodo*) malloc(sizeof(struct nodo)); 
	new->val = 20;
	new->next = NULL;
	testa->next = new;
	
	visualizza(testa);
	
	return 0;
}

// Visualizzo la lista
void visualizza(struct nodo *l)
{
	while(l != NULL) { 	
		printf("%d -> ", l->val); 
	  	l = l->next; 
	}
	printf("NULL \n");
}

// Visualizzo la lista con una funzione ricorsiva
void visualizza_r(struct nodo *p)
{
	if (p != NULL) {
		printf("%d -> ", p->val); 
		visualizza_r(p->next);
	} else {
		printf("NULL \n");
	}
}

