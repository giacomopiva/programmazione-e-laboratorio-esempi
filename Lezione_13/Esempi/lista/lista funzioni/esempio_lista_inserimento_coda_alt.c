#include <stdio.h>
#include <stdlib.h>

#define MAX_DIM 5

struct nodo {
	int val;
	struct nodo *next;
};

void visualizza(struct nodo *);

int main()
{
	struct nodo *testa, *new;
	int i, array[MAX_DIM] = {1,2,3,4,5};
	
	// Inizializzo il puntatore della testa a NULL
	testa = NULL;
		
	// Riempio la lista con gli elementi presenti nell'array	
	// Per ogni elemento presente nell'array
	for (i=0; i<MAX_DIM; i++) {			
		// Se è il primo elemento
		if (testa == NULL) {
			new = (struct nodo*) malloc(sizeof(struct nodo));
			testa = new;	// Assegno la testa della lista
		} else {
			new->next = (struct nodo*) malloc(sizeof(struct nodo));
			new = new->next;	// Mi sposto al prossimo nodo
		}
		new->val = array[i];
		new->next = NULL;	
	}

	visualizza(testa);
	
	return 0;
}

// Visualizzo la lista
void visualizza(struct nodo *l)
{
	while(l != NULL) { 	
		printf("%d ", l->val); 
	  l = l->next; 
	 }
	 printf("\n");
}
