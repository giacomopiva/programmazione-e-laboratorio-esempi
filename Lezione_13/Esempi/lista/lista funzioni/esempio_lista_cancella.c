#include <stdio.h>
#include <stdlib.h>

#define MAX_DIM 5

typedef struct _nodo {
	float val;
	struct _nodo *next;
} nodo;

nodo* crealista();
void concatena(nodo*,nodo*);
nodo* cerca(nodo*, float);
void cancella(nodo**, float);
void visualizza(nodo *);

int main()
{
	nodo *alista, *blista, *p;
	float todel;
	
	alista = crealista();
	visualizza(alista);
	
	blista = crealista();
	visualizza(blista);
	
	concatena(alista, blista);
	visualizza(alista);
	
	printf("\nInserisci il valore da cercare: ");
	scanf("%f", &todel);
	
	p = cerca(alista, todel);

	if (p != NULL) {
		cancella(&alista, todel);
	} else {
		printf("Il numero non è contenuto nella lista.\n");
	}

	visualizza(alista);
		
	return 0;
}

nodo* crealista()
{
	nodo *new, *testa;
	float num;
	int i;
	
	testa = NULL;
	
	for (i=0; i<5; i++) {
		new = (nodo *) malloc(sizeof(nodo)); 
		printf("Inserisci il %d elemento: ", i+1);
		scanf("%f", &num);
		new->val = num; 
		new->next = testa; 
		testa = new; 
	}
	return testa;
}

void concatena(nodo *a, nodo *b)
{
	while(a->next != NULL)
		a = a->next;
	a->next = b;
}

nodo* cerca(nodo *l, float v)
{
	while(l != NULL) { 	
	  if (l->val == v)
			return l;
		l = l->next; 
	 }
	 return NULL;
}

void cancella(nodo **lista, float v)
{
	nodo *p, *q;
	p = *lista;
	q = NULL;
	while(p != NULL) { 	
		if (p->val == v) {
	  	if (p == *lista) {
	  		*lista = (*lista)->next;
			} else {
				q->next = p->next;
			}
			free(p);
			return;
		}
		q = p;
		p = p->next;
	}
}

void visualizza(nodo *l)
{
	while(l != NULL) { 	
		printf("%.1f ", l->val); 
	  l = l->next; 
	 }
	 printf("\n");	 
}








