#include <stdio.h>
#include <stdlib.h>

#define MAX_DIM 5

typedef struct _nodo {
	float val;
	struct _nodo *next;
} nodo;

nodo* crealista();
nodo* cerca(nodo*, float);
void visualizza(nodo *);

int main()
{
	nodo *alista, *p;
	
	alista = crealista();
	visualizza(alista);
	
	p = cerca(alista, 5.0);
	if (p != NULL)
		printf("--> %.2f, %p \n", p->val, p);
	
	return 0;
}

nodo* crealista()
{
	nodo *new, *testa;
	float num;
	int i;
	
	testa = NULL;
	
	for (i=0; i<5; i++) {
		new = (nodo *) malloc(sizeof(nodo)); 
		printf("Inserisci il %d elemento: ", i+1);
		scanf("%f", &num);
		new->val = num; 
		new->next = testa; 
		testa = new; 
	}
	return testa;
}

nodo* cerca(nodo *l, float v)
{
	while(l != NULL) { 	
	  if (l->val == v)
			return l;
		l = l->next; 
	 }
	 return NULL;
}

void visualizza(nodo *l)
{
	while(l != NULL) { 	
		printf("%.1f ", l->val); 
	  l = l->next; 
	 }
	 printf("\n");	 
}








