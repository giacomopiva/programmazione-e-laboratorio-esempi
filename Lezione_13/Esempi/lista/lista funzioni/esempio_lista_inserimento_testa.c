#include <stdio.h>
#include <stdlib.h>

#define MAX_DIM 5

struct nodo {
	int val;
	struct nodo *next;
};

void visualizza(struct nodo *);

int main()
{
	struct nodo *testa, *new;
	int i, array[MAX_DIM] = {1,2,3,4,5};
	
	testa = NULL;
	
	// Riempio la lista con gli elementi presenti nell'array	
	// Per ogni elemento presente nell'array
	for (i=0; i<MAX_DIM; i++) {
		// Alloco un nuovo nodo
		new = (struct nodo*) malloc(sizeof(struct nodo));	
		
		// Inserisco il valore nella struct
		new->val = array[i];	
		
		// Concateno il nuovo elemento in testa  
		new->next = testa; // new->next = NULL;
		
		// Assegno la nuova testa della lista
		testa = new;
	}
	
	// Visualizzo la lista
	visualizza(testa);
	
	return 0;
}

void visualizza(struct nodo *l)
{
	while(l != NULL) { 	
		printf("%d ", l->val); 
	  l = l->next; 
	 }
	 printf("\n");	 
}
