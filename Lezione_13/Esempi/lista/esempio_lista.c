/**
 * Autore: Giacomo Piva <giacomo.piva@unife.it>
 * Descrizione: In questo esempio, vengono creati 
 * due nodi di una lista e vengono concatenati
 * fra loro. 
 */

#include <stdio.h>
#include <stdlib.h>

struct nodo {
	int val;
	struct nodo *next;
};

int main()
{
	// Dichiaro un puntatore a nodo che sarà la testa della mia lista
	struct nodo *testa, *new;
	
	// Alloco in memoria il puntatore al primo nodo della lista
	new = (struct nodo*) malloc(sizeof(struct nodo));

	// Inserisco i dati nel nodo
	new->val = 7;

	// Pongo a NULL il puntatore al prossimo nodo
	// NULL indica che non c'è un prossimo elemento
	new->next = NULL; 	
	
	// Collego la testa della mia lista al nodo appena creato.
	testa = new;
	
	// A questo punto il mio nodo è "salvo" perchè ho il puntatore testa che me lo referenzia.
	// Posso riciclare new per un secondo nodo:
	new = (struct nodo*) malloc(sizeof(struct nodo));
	new->val = 15;
	new->next = NULL; 	
		
	// Collego i due nodi...
	testa->next = new;
	
	// Arrivato a questo punto, posso ancora temporaneamente accedere al secondo nodo, 
	// ma una volta allocato un terzo nodo, non avrei più la possibilità di farlo.
	// L'unico modo per poter accedere ad un nodo è attraverso il suo puntatore.
	// Per ogni nodo il suo puntatore è memorizzato nel nodo che lo prcede.
	// Per poter accedere al secondo nodo, l'unico modo è scorrere la lista fino a 
	// trovare il nodo che contiene il suo puntatore.
	//
	// Supponiamo di voler scorrere la lista...
	//
	struct nodo *l;		// Dichiaro un puntatore temporaneo per scorrere la lista
	l = testa;			// Inizializzo il puntatore al primo nodo della lista
	
	while(l != NULL) {	// Finchè il mio puntatore non punta a NULL ...
		// ...
	  	l = l->next;	// Passo al prossimo elemento
	 }

	return 0;
}
