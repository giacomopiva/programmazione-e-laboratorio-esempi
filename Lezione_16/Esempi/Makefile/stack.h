#ifndef _stack
#define _stack

/* Definizione del nodo dello stack */
typedef struct _operando {
	float val;
	struct _operando *next;
} operando;

/* Inserisco un elemento nello stack */
void push(operando**, float);

/* Prelevo un elementop dallo stack */
operando* pop(operando**);

/* Visualizzo il contenuto dello stack */
void visualizza(operando *);

#endif
