#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stack.h"
	
operando* pop(operando **testa)
{
	operando *p;
	p = NULL;
	if (*testa != NULL) {
		p = *testa;
		*testa = (*testa)->next;
		p->next = NULL;
	} 
	return p;
}

void push(operando **testa, float value) 
{
	operando *new;
	new = (operando*) malloc(sizeof(operando));	
	new->val = value;
	new->next = *testa;
	*testa = new;
}

void visualizza(operando *testa)
{
	while (testa != NULL) { 	
		printf("%.2f ", testa->val); 
	  testa = testa->next; 
	 }
	 printf("\n");
}
