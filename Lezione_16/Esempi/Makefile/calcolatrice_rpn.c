#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stack.h"

float calcola(operando*, operando*, char);
	
int main()
{
	operando *stack_operandi, *leftop, *rightop;
	float res;
	char *token, op;
	
	stack_operandi = NULL;
	token = NULL;
	char expr[255] = "31 4 + 5 -"; 
	
	token = strtok(expr, " ");
	
	while(token != NULL) {
		if (atof(token)) {
			push(&stack_operandi, atof(token));
		} else {
			op = token[0];
			rightop = pop(&stack_operandi);
			leftop = pop(&stack_operandi);
			res = calcola(leftop, rightop, op);
			push(&stack_operandi, res);
		}
		token = strtok(NULL, " ");
	}
	visualizza(stack_operandi);
	return 0;
}

float calcola(operando *operando1, operando *operando2, char op)
{
	switch(op) {
		case '+':
			return operando1->val + operando2->val;
		break;
		case '-':
			return operando1->val - operando2->val;
		break;
		case '*':
			return operando1->val * operando2->val;		
		break;
		case '/':
			return operando1->val / operando2->val;		
		break;
		default:
		printf("%c: Operazione non consentita", op);
		exit(1);
	}
}
	