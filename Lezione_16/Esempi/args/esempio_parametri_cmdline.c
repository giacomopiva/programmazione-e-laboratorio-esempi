#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	FILE *fp;
	char *filename;
	
	filename = argv[1]; 	// filename NON è una copia di argv[1] 
	
	if (filename == NULL) {
		printf("Non è stato specificato un file.\n");
		exit(1);
	} else {
		printf("Proseguo con il salvataggio sul file %s.\n", filename);
		fp = fopen(filename, "w");	
		if (fp != NULL) {
			fprintf(fp, "Mario;Rossi;+393354562345\n");
			if (fclose(fp) == EOF) {
				printf("Si è verificato un errore durante la chiusutra del file\n");
				exit(1);
			}
		} else {
			printf("Non ho potuto aprire il file\n");
			exit(1);
		}
	}	
	return 0;
}
