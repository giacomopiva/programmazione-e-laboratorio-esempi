/**
 * Autore: Giacomo Piva <giacomo.piva@unife.it>
 * Descrizione: In questo esempio vengono 
 * utilizzat ele funzioni fopen() ed fclose()
 * per aprire in modo corretto un file. 
 * Viene usata la funzione fprintf() per scrivere
 * una riga di teso su un file.
 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
	FILE *fp;

	// Apro il file
	fp = fopen("pippo.txt", "w");	
	
	// Controlo che l'apertura sia avvenuta senza errori	
	if (fp != NULL) {
		// Scrivo su file... 
		fprintf(fp, "Ho aperto il file ... \n");
		
		// Chiudo il file
		if (fclose(fp) == EOF) {
			// Errore durante la fclose
			printf("Si è verificato un errore durante la chiusura del file\n");
			exit(1);
		}
	} else {
		// Errore durante la fopen
		printf("Non ho potuto aprire il file\n");
		exit(1);
	}
	
	return 0;
}
