/**
 * Autore: Giacomo Piva <giacomo.piva@unife.it>
 * Descrizione: In questo esempio vengono 
 * utilizzate le funzioni fprintf() ed fgets()
 * per scrivere e leggere su un file di testo.
 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
	FILE *fp;
	char nome[255], cognome[255];
	int anni;
	char buf[255];
	
	// Apro il file
	fp = fopen("pippo.txt", "w");	
	
	if (fp != NULL) {
		// Scrivo sul file ...
		fprintf(fp, "Giacomo Piva 32\n");
		fprintf(fp, "Mario Rossi 35\n");
		
		if (fclose(fp) == EOF) {
			printf("Errore durante la chiusutra del file\n");
			exit(1);
		}
	} else {
		printf("Non ho potuto aprire il file\n");
		exit(1);
	}

	fp = fopen("pippo.txt", "r");	
	if (fp != NULL) {
		while (!feof(fp)) {
			// Posso usare la fscanf() se so con certezza come è 
			// strutturato il file. 
			// Attenzione! La fscanf() vuole anche il \n per il 
			// terminaore di linea
			//
			//fscanf(fp, "%s %s %d\n", nome, cognome, &anni); 
			//printf("%s %s %d\n", nome, cognome, anni);
			
			// Usando la fgets(), devo poi procedere al parsing della 
			// stringa nel memorizzata in buf[].
			//
			if (fgets(buf, 256, fp) != NULL) {
				// Qui posso usare buf per le successive 
				// elaborazioni, ad esempio posso fare il
				// parsing della stringa con strtok
				printf("%s", buf);
			}
		}

		if (fclose(fp) == EOF) {
			printf("Errore durante la chiusutra del file\n");
			exit(1);
		}
	} else {
		printf("Non ho potuto aprire il file\n");
		exit(1);
	}

	return 0;
}
