#include <stdio.h>
#include <string.h>

int main(void)
{
	// Creo una struttura dati 
	// che descrive la data
	struct date {
		int day;
		char month[20];
		int year;
	};

	// Dichiaro una variabile di 
	// tipo "struct date"
	struct date today;

	// Assegno i valori ai campi
	// della struttura
	today.day = 16;
	strcpy(today.month, "Novembre");
	today.year = 2015;
	
	printf("%d %s %d \n", today.day, today.month, today.year);
	
	return 0;
}
