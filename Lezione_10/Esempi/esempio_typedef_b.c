#include <stdio.h>
#include <string.h>

int main(void)
{
	typedef struct _date {
		int day;
		char month[20];
		int year;
	} date;
		
	date today;

	today.day = 16;
	strcpy(today.month, "Novembre");
	today.year = 2015;
	
	printf("%d %s %d \n", today.day, today.month, today.year);
	
	return 0;
}
