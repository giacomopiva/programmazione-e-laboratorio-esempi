#include <stdio.h>

int main(void)
{	
	typedef enum _colore { 
		BLU, 
		GIALLO, 
		MAGENTA, 
		NERO 
	} colore;
	
	colore inchiostro = NERO; 
	
	switch (inchiostro) {
		case BLU:
			printf("L'inchiostro è blu\n");
		break;
		
		case GIALLO:
			printf("L'inchiostro è giallo\n");
		break;
		
		case MAGENTA:
			printf("L'inchiostro è magenta\n");
		break;
		
		case NERO:
			printf("L'inchiostro è nero\n");
		break;
	}

	return 0;
}
