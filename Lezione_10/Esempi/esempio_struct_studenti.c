#include <stdio.h>
#include <string.h>

#define MAX_STUDENTI 2
#define MAX_VOTI 3

int main(void)
{
	// Dichiaro la struttura dati
	struct studente {
		char nome[20];
		char cognome[20];
		int anno;
		int voti[MAX_VOTI];
	};
	
	// Dichiaro 2 variabili per gli indici
	int i, j;
	
	// Dichiaro un array di strutture
	struct studente studenti[MAX_STUDENTI];
	
	// Con un primo ciclo for, ripeto le operazioni
	// per gli N (cioè MAX_STUDENTI) studenti
	for (i=0; i<MAX_STUDENTI; i++) {		
		// Chiedo all'utente di inserire nome e 
		// cognome dello studente
		printf("Inserire nome cognome: ");
		scanf("%s %s", studenti[i].nome, studenti[i].cognome);

		// Chiedo all'utente di inserire l'anno 
		// di corso dello studente
		printf("Inserire anno di corso: ");
		scanf("%d", &studenti[i].anno);

		// Con un ciclo for leggo i voti
		// memorizzandoli nel campo "voti"		
		for (j=0; j<MAX_VOTI; j++) {
			printf("Inserire %d° voto: ", j+1);
			scanf("%d", &studenti[i].voti[j]);
		}
	}
	
	// Una volte riempita la nostra struttura dati
	// proseguo con i calcoli ...
	// Per ogni studente ...
	for (i=0; i<MAX_STUDENTI; i++) { 
		int somma = 0; // inizializzo la somma 
		// Accedo ale informazioni dello studente
		// attraverso studenti[i] ...
		// Prer ogni voto ...
		for (j=0; j<MAX_VOTI; j++) {
			// Accumulo su somma il singolo voto
			somma += studenti[i].voti[j];
		}

		// Al termine del ciclo per il calcolo della media
		// del singolo studente, visualizzo la media
		float media = (float) somma / MAX_VOTI;
		printf("Media di %s %s: %.1f\n", studenti[i].nome, studenti[i].cognome, media);
	}
	
	return 0;
}
