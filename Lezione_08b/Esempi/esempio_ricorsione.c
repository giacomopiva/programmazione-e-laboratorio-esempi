#include <stdio.h>

long int fatt(int);

int main()
{		
	int n, i;
	long int fattoriale;
	
	printf("Inserisci un numero intero ");
	scanf("%d", &n);
	
	// Calcolo il fattoriale di n con un ciclo for
	fattoriale = 1;
	for(i=n; i>1; i--) 
		fattoriale *= i;
	
	printf("Il fattoriale di %d e\' %ld \n\n", n, fattoriale);
	
	// Calcolo il fattoriale di n con una funzione ricorsiva
	fattoriale = fatt(n);
	
	printf("Il fattoriale di %d e\' %ld \n\n", n, fattoriale);
		
	return 0;
}

long int fatt(int n)
{	
	if (n==0) {
  		printf("1\n");
		return 1;
	} else {
		printf("fatt(%d) * ",n);
		return (n * fatt(n-1));
	}
}
